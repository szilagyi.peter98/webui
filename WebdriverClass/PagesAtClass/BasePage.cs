﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public abstract class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        public string GetUrl()
        {
            return Driver.Url;
        }

        public abstract List<IWebElement> GetElements();
    }
}
