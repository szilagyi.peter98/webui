﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class MainWikiPage : BasePage
    {
        private IWebElement WelcomeLinkAndText => Driver.FindElement(By.CssSelector("a[title='Wikipédia:Üdvözlünk, látogató!']"));

        public MainWikiPage(IWebDriver driver) : base(driver){}

        public override List<IWebElement> GetElements()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            wait.Until(d => WelcomeLinkAndText);

            return new List<IWebElement>()
            {
               WelcomeLinkAndText,
            };
        }
    }
}
