﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class MainPage_Neumann : BasePage
    {
        private IWebElement MainWiki => Driver.FindElement(By.CssSelector("a.mw-wiki-logo"));
        private IWebElement Budapest => Driver.FindElement(By.CssSelector("a[href='/wiki/Budapest']"));
        private IWebElement NeumannEdit => Driver.FindElement(By.CssSelector("a[title='A lap szerkesztése [alt-shift-e]']"));



        public MainPage_Neumann(IWebDriver webDriver) : base(webDriver) { }

        public static MainPage_Neumann Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://hu.wikipedia.org/wiki/Neumann_J%C3%A1nos";
            return new MainPage_Neumann(webDriver);
        }

        public MainWikiPage GetMainWikiPage()
        {
            MainWiki.Click();
            return new MainWikiPage(Driver);
        }

        public BudapestPage GetBudapestPage()
        {
            Budapest.Click();
            return new BudapestPage(Driver);
        }

        public NeumannEditPage GetNeumannEditPage()
        {
            NeumannEdit.Click();
            return new NeumannEditPage(Driver);
        }

        public override List<IWebElement> GetElements()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            wait.Until(d => MainWiki);
            wait.Until(d => Budapest);
            wait.Until(d => NeumannEdit);


            return new List<IWebElement>()
            {
                MainWiki,
                Budapest,
                NeumannEdit
            };
        }



    }
}
