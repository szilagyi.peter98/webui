﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class BudapestPage : BasePage
    {
        private IWebElement NeighborhoodsInCity => Driver.FindElement(By.CssSelector("span[id='Városrészek']"));
        public BudapestPage(IWebDriver driver) : base(driver) { }

        public override List<IWebElement> GetElements()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(25));
            wait.Until(d => NeighborhoodsInCity);

            return new List<IWebElement>()
            {
                NeighborhoodsInCity
            };
        }
    }
}
