﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebdriverClass.PagesAtClass
{
    public class NeumannEditPage : BasePage
    {
        private IWebElement WarningIconInEditTools => Driver.FindElement(By.CssSelector("a[class='oo-ui-tool-link'][tabindex='0'][title='Szerkesztési bevezetők']"));
        //private IWebElement StartToEditButton => Driver.FindElement(By.CssSelector("a[role='button'][tabindex='0'][rel='nofollow'][class='a']"));
        private IWebElement StartToEditButton => Driver.FindElement(By.XPath("//*[contains(@class,'oo-ui-widget oo-ui-widget-enabled oo-ui-buttonElement oo-ui-labelElement oo-ui-flaggedElement-progressive oo-ui-flaggedElement-primary oo-ui-buttonWidget oo-ui-actionWidget oo-ui-buttonElement-framed')]/a"));
        private List<IWebElement> CloseMessageBox => Driver.FindElements(By.CssSelector("a[title='Bezárás']")).ToList();
        public NeumannEditPage(IWebDriver driver) : base(driver) { }

        public override List<IWebElement> GetElements()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(75));
            var readytoedit = wait.Until(d => StartToEditButton);
            readytoedit.Click();

            wait.Until(d => CloseMessageBox);

            //foreach (var item in CloseMessageBox)
            //{
            //    try
            //    {
            //        item?.Click();
            //    }
            //    catch (Exception)
            //    {
            //        //nothing, not clickable..
            //    }
            //   
            //}

            wait.Until(d => WarningIconInEditTools);

            return new List<IWebElement>()
            {
                WarningIconInEditTools,
            };
        }
    }
}
