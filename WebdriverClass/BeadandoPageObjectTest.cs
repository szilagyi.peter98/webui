﻿using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {

        [Test, TestCaseSource("GetTestData")]
        public void NeumannJanosWikiPageTest(string method, string expectedUrl)
        {
            MainPage_Neumann landing = MainPage_Neumann.Navigate(Driver);
            MethodInfo getterMethod = typeof(MainPage_Neumann).GetMethod(method);
            BasePage page = getterMethod.Invoke(landing, null) as BasePage;

            
            foreach (var webElement in page.GetElements()){
                Assert.IsTrue(webElement.Displayed);
            }

            page.GetElements().ForEach(webElement => Assert.IsTrue(webElement.Displayed));

            //Assert.AreEqual(expectedUrl, page.GetUrl());  
        }

        static IEnumerable GetTestData()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");
            return
                from vars in doc.Descendants("TestData")
                let MethodName = vars.Attribute("NameOfMethod").Value
                let Url = vars.Attribute("Url").Value
                select new object[] { MethodName, Url };
        }


    }
}
